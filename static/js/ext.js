// For background/context example
chrome.runtime.onMessage.addListener(function(msg, sender, response){
  if(msg.request === "clip") {
    var fragment = window.getSelection().getRangeAt(0).cloneContents();
    var div = document.createElement("div");
    div.appendChild(fragment);
    response(div.innerHTML);
  }
});

// For Clip example
chrome.runtime.onMessage.addListener(function(msg, sender, response){
  if(msg.request === "scrape") {
    var elements = document.getElementsByTagName(msg.elemType);
    var results = [];
    for(var i=0; i < elements.length; ++i) {
      results.push(elements[i].innerText);
    }
    response(results);
  }
});

// For text replacement example
(function(){
  var replacement_rules = [];

  // Used by snippet example
  function loadRules() {
    chrome.storage.sync.get("replacement_rules", function(obj){
      var rules = obj.replacement_rules || [];
      var result = [];
      for(idx in rules) {
        var rule = rules[idx];
        result.push({
          matcher:     new RegExp(rule.pattern+"(\n| |$)"),
          replacer:    new RegExp(rule.pattern),
          replacement: rule.replacement
        });
      }
      replacement_rules = result;
    });
  }

  loadRules();
  chrome.storage.onChanged.addListener(loadRules);

  function setEndOfContenteditable(contentEditableElement) {
    var range,selection;
    range = document.createRange();//Create a range (a range is a like the selection but invisible)
    range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
    range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
    selection = window.getSelection();//get the selection object (allows you to change selection)
    selection.removeAllRanges();//remove any selections already made
    selection.addRange(range);//make the range you have just created the visible selection
  }

  document.body.addEventListener("keyup", function(event){
    var trg = event.target;
    if(trg.tagName === "INPUT" && trg.value) {
      for(idx in replacement_rules) {
        var rule = replacement_rules[idx];
        if(trg.value.match(rule.matcher)) {
          trg.value = trg.value.replace(rule.replacer, rule.replacement);
        }
      }
    } else if (trg.contentEditable === "true") {
      for(idx in replacement_rules) {
        var rule = replacement_rules[idx];
        if(trg.innerText.match(rule.matcher)) {
          trg.innerHTML = trg.innerHTML.replace(rule.replacer, rule.replacement);
          // Use a timeout to work around slack
          setTimeout(function(){
            setEndOfContenteditable(trg);
          }, 10);
        }
      }
    };
  });
})();
