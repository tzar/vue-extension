// For Context menu -> to markdown example
(function(){
  function copyTextToClipboard(text) {
    var copyFrom = document.createElement("textarea");
    copyFrom.textContent = text;
    var body = document.getElementsByTagName('body')[0];
    body.appendChild(copyFrom);
    copyFrom.select();
    document.execCommand('copy');
    body.removeChild(copyFrom);
  }
  chrome.contextMenus.create({
    id: "42",
    title: "Copy as Markdown",
    contexts: ["selection"],
    onclick: function(info, tab) {
      chrome.tabs.sendMessage( tab.id, {request: "clip"}, function(selected_html) {
        copyTextToClipboard(toMarkdown(selected_html));
      });
    }
  });
})();
