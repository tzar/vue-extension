import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Clock from '@/components/Clock'
import Clip from '@/components/Clip'
import Snippets from '@/components/Snippets'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/clock',
      name: "Clock",
      component: Clock
    },
    {
      path: '/clip',
      name: "Clip",
      component: Clip
    },
    {
      path: '/snippets',
      name: "Snippets",
      component: Snippets
    }
  ]
})
